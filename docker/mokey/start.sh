#!/bin/bash
echo login.pilotescola.digital > /etc/hostname
sysctl -w kernel.hostname=login.pilotescola.digital
# Create necessary keytab directory
/usr/bin/mkdir -p /etc/mokey/keytab

# Enroll container to IPA
/usr/sbin/ipa-client-install -U --principal admin \
                                --password "$IPA_ADMIN_PWD" \
                                --domain $DOMAIN \
                                --realm $(echo "$DOMAIN" | tr '[:lower:]' '[:upper:]') \
                                --server ipa.$DOMAIN \
                                --force-join \
                                --no-ntp \
                                --no-ssh \
                                --no-sshd \
                                --no-nisdomain \
                                --noac \
                                --no-sssd \
                                --log-file /ipa-client-install.log
# kinit admin user
echo "$IPA_ADMIN_PWD" | kinit admin
# Retrieve keytab for mokeyapp
/usr/sbin/ipa-getkeytab -s ipa.$DOMAIN -p mokeyapp -k /etc/mokey/keytab/mokeyapp.keytab

# Run mokey application
/usr/bin/mokey --debug server
