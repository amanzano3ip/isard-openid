#!/bin/bash
. ../../.env

APP=moodle

APP_CALLBACK=https://$APP.$DOMAIN/auth/oidc/
APP_ID=$APP
APP_SECRET=Sup3rS3cr3t

docker-compose  exec hydra \
    hydra clients create \
    --endpoint http://hydra:4445/ \
    --id $APP_ID \
    --secret $APP_SECRET \
    --grant-types client_credentials,authorization_code,refresh_token \
    --token-endpoint-auth-method client_secret_post \
    --response-types code \
    --scope openid,offline,profile,email \
    --callbacks $APP_CALLBACK


docker-compose exec hydra \
    hydra token client \
    --endpoint http://hydra:4444/ \
    --client-id $APP_ID \
    --client-secret $APP_SECRET

# TO REMOVE CLIENT
# docker-compose exec hydra hydra clients delete $APP --endpoint http://hydra:4445/
